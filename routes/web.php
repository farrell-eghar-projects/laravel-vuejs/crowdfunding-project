<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Route::middleware('email.verif')->group(function() {
//     Route::get('/route-1', 'HomeController@verifEmail');
// });

// Route::middleware(['email.verif', 'admin'])->group(function() {
//     Route::get('/route-2', 'HomeController@adminDanVerifEmail');
// });

// Route::get('/', function() {
//     return view('app');
// });

// Route::get('/{any?}', function() {
//     return 'Masuk ke sini';
// })->where('any', '.*');

Route::view('/{any?}', 'app')->where('any', '.*');
