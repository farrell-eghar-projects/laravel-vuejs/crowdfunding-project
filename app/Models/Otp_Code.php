<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Otp_Code extends Model
{
    use Uuid;

    protected $table = 'otp_codes';

    protected $guarded = [];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
