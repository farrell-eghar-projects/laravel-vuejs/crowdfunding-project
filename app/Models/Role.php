<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Role extends Model
{
    use Uuid;

    protected $table = 'roles';

    protected $guarded = [];

    public function users() {
        return $this->hasMany(User::class);
    }

    public function isAdmin() {
        if($this->peran == 'admin') {
            return true;
        }
        return false;
    }
}
