<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Otp_Code as Otp;
use App\Models\User;
use App\Events\RegenerateOtpEvent;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $otp = Otp::where('user_id', $request->user()->id)->first();

        if (!$otp) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Anda Telah Melakukan Verifikasi Email!',
                'user' => $request->user()
            ], 200);
        }

        $otp->update([
            'kode' => rand(100000, 999999),
            'kadaluarsa' => Carbon::now()->addMinutes(15),
        ]);

        $user = $otp->user;

        event(new RegenerateOtpEvent($user));

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Kode OTP Telah Terkirim! Silahkan Cek Email Anda.',
            'user' => $request->user()
        ]);
    }
}
