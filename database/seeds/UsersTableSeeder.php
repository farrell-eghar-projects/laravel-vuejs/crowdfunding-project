<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'id' => \Ramsey\Uuid\Uuid::uuid4()->toString(),
                'name' => 'Farrell',
                'email' => 'farrell@gmail.com',
                'email_verified_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'password' => Hash::make('farrell123'),
                'role_id' => '9e3ecdc1-c743-4769-841c-df8fe1c296b1',
                'remember_token' => '0rnrB608Nq',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'id' => \Ramsey\Uuid\Uuid::uuid4()->toString(),
                'name' => 'eghar',
                'email' => 'eghar@gmail.com',
                'email_verified_at' => null,
                'password' => Hash::make('eghar123'),
                'role_id' => '9e3ecdc1-c743-4769-841c-df8fe1c296b1',
                'remember_token' => '0rnrB608Nq',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'id' => \Ramsey\Uuid\Uuid::uuid4()->toString(),
                'name' => 'Farrell Eghar',
                'email' => 'farrelleghar@gmail.com',
                'email_verified_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'password' => Hash::make('farrelleghar123'),
                'role_id' => '17596a83-c57a-4e7b-8cc3-7fcd0332ae93',
                'remember_token' => '0rnrB608Nq',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'id' => \Ramsey\Uuid\Uuid::uuid4()->toString(),
                'name' => 'Eghar Farrell',
                'email' => 'egharfarrell@gmail.com',
                'email_verified_at' => null,
                'password' => Hash::make('egharfarrell123'),
                'role_id' => '17596a83-c57a-4e7b-8cc3-7fcd0332ae93',
                'remember_token' => '0rnrB608Nq',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);
    }
}
